﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Hospital._Default" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <dx:BootstrapImage ID="BootstrapImage1" runat="server" ImageUrl="~/Uploads/hospital.png" Height="231px" Width="995px"></dx:BootstrapImage>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>مستشفـــى الشفـــــاء</h2>
            <p>
               نقدم لكم رعاية طبية متكاملة
            </p>
            
        </div>
        
    </div>

</asp:Content>
