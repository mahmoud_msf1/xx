﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Hospital.DAL
{
    public class DataAccessLayer
    {
        SqlConnection con;
        public DataAccessLayer()
        {
            con = new SqlConnection(@"Server=MAHMOUD-PC\SQLEXPRESS; Database=MyHospitalDB;Integrated Security=true");
        }

        //method to open connection
        public void Open()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //method to close connection
        public void Close()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }


        //method to read data from database
        public DataTable SelectData(string StoredProcedureName, SqlParameter[] Param)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandText = StoredProcedureName;
            sqlcmd.Connection = con;
            if (Param != null)
            {
                sqlcmd.Parameters.AddRange(Param);
            }
            SqlDataAdapter da = new SqlDataAdapter(sqlcmd);//to execute sqlcmd
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        //method to Insert,Update and Delete Data from Database
        public void ExecuteCommand(string StoredProcedureName, SqlParameter[] Param)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandText = StoredProcedureName;
            sqlcmd.Connection = con;
            if (Param != null)
            {
                sqlcmd.Parameters.AddRange(Param);
            }
            sqlcmd.ExecuteNonQuery();
        }
    }
}