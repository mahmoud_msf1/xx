﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Hospital.BL
{
    public class clsRegister
    {
        public void NewRegister(string EnglishName,string ArabicName,string Gender,string Nationality,
                                string DateOfEntry,string DateOfExit/*,byte img*/,string Department,
                                string Doctor,string Phone,string Mobile)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[10];

            param[0] = new SqlParameter("@EnglishName",SqlDbType.NVarChar,50);
            param[0].Value = EnglishName;

            param[1] = new SqlParameter("@ArabicName", SqlDbType.NVarChar, 50);
            param[1].Value = ArabicName;

            param[2] = new SqlParameter("@Gender", SqlDbType.NChar, 10);
            param[2].Value = Gender;

            param[3] = new SqlParameter("@Nationality", SqlDbType.NVarChar, 50);
            param[3].Value = Nationality;

            param[4] = new SqlParameter("@DateOfEntry", SqlDbType.NVarChar, 50);
            param[4].Value = DateOfEntry;

            param[5] = new SqlParameter("@DateOfExit", SqlDbType.NVarChar, 50);
            param[5].Value = DateOfExit;

            //param[6] = new SqlParameter("@IDCart", SqlDbType.Image);
            //param[6].Value = img;

            param[6] = new SqlParameter("@Department", SqlDbType.NVarChar, 50);
            param[6].Value = Department;

            param[7] = new SqlParameter("@Doctor", SqlDbType.NVarChar, 50);
            param[7].Value = Doctor;

            param[8] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[8].Value = Phone;

            param[9] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[9].Value = Mobile;

            DAL.ExecuteCommand("ProcNewRegister", param);
            DAL.Close();


        }

        public DataTable GetPatient()
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DataTable dt = new DataTable();
            dt= DAL.SelectData("ProcGetPatient", null);
            DAL.Close();
            return dt;
        }

        public DataTable GetSelectedPatient(int Id)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@id", SqlDbType.Int);
            param[0].Value = Id;

            
            dt= DAL.SelectData("ProcGetSelectedPatient", param);
            DAL.Close();
            return dt;

        }

        public DataTable Search(string Q)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@q", SqlDbType.NVarChar,50);
            param[0].Value = Q;


            dt = DAL.SelectData("ProcSearch", param);
            DAL.Close();
            return dt;

        }


        public void EditData(string EnglishName, string ArabicName, 
                              string DateOfEntry, string DateOfExit, string Department,
                              string Doctor, string Phone, string Mobile)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[8];

            param[0] = new SqlParameter("@EnglishName", SqlDbType.NVarChar, 50);
            param[0].Value = EnglishName;

            param[1] = new SqlParameter("@ArabicName", SqlDbType.NVarChar, 50);
            param[1].Value = ArabicName;

            param[2] = new SqlParameter("@DateOfEntry", SqlDbType.NVarChar, 50);
            param[2].Value = DateOfEntry;

            param[3] = new SqlParameter("@DateOfExit", SqlDbType.NVarChar, 50);
            param[3].Value = DateOfExit;

            param[4] = new SqlParameter("@Department", SqlDbType.NVarChar, 50);
            param[4].Value = Department;

            param[5] = new SqlParameter("@Doctor", SqlDbType.NVarChar, 50);
            param[5].Value = Doctor;

            param[6] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[6].Value = Phone;

            param[7] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[7].Value = Mobile;

            DAL.ExecuteCommand("ProcEditData", param);
            DAL.Close();


        }


        public void AddDetailes(string TypeOfDisease, string DateOfInjury,
                             string Endemic, string Genetics, int PatientID
                             )
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@TypeOfDisease", SqlDbType.NVarChar, 50);
            param[0].Value = TypeOfDisease;

            param[1] = new SqlParameter("@DateOfInjury", SqlDbType.NVarChar, 50);
            param[1].Value = DateOfInjury;

            param[2] = new SqlParameter("@Endemic", SqlDbType.NVarChar, 15);
            param[2].Value = Endemic;

            param[3] = new SqlParameter("@Genetics", SqlDbType.NVarChar, 15);
            param[3].Value = Genetics;

            param[4] = new SqlParameter("@IDPatient", SqlDbType.Int);
            param[4].Value = PatientID;


            DAL.ExecuteCommand("ProcAddDetailes", param);
            DAL.Close();


        }

        public DataTable GetLastIDPatient()
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DataTable dt = new DataTable();
            dt = DAL.SelectData("ProcGetLastIDPatient", null);
            DAL.Close();
            return dt;
        }

    }
}