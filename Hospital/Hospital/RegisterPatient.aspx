﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterPatient.aspx.cs" Inherits="Hospital.RegisterPatient" %>
<%@ Register assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.Bootstrap.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.Bootstrap" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    






    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified table table-condensed ">
                <tr style="margin: 25px;">
                    <td colspan="3">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" class="page-header" Font-Bold="True" Font-Size="XX-Large" Font-Underline="True" style="text-align: center; margin: 15px;" Text="Register Patient" Theme="Material">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr style="margin: 15px;">
                    <td colspan="3" style="margin: 15px;">
                        <dx:ASPxLabel ID="lblMsg" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Large" ForeColor="Red">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Name in arabic">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 10px; margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtNameArabic" runat="server" Height="16px" Width="359px">
                            <ValidationSettings Display="Dynamic" EnableCustomValidation="True" ErrorDisplayMode="Text" ErrorText="must enter name in arabic">
                                <RegularExpression ValidationExpression="^[\u0621-\u064A\u0660-\u0669 ]+$" />
                                <RequiredField ErrorText="must enter name in arabic" IsRequired="True" />
                            </ValidationSettings>
                            <Border BorderStyle="Solid" />
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Name in English">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtNameEnglish" runat="server" Width="359px">
                            <ValidationSettings ErrorText="must enter your name in english">
                                <RequiredField IsRequired="True" ErrorText="must enter your name in english" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Gender">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxRadioButtonList ID="rdoGender" runat="server" Height="25px" RepeatDirection="Horizontal" SelectedIndex="0" Width="209px">
                            <Items>
                                <dx:ListEditItem Selected="True" Text="Male" Value="Male" />
                                <dx:ListEditItem Text="Female" Value="Female" />
                            </Items>
                        </dx:ASPxRadioButtonList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Nationality">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtNationality" runat="server" Width="359px">
                            <ValidationSettings ErrorText="must enter nationality">
                                <RequiredField IsRequired="True" ErrorText="must enter nationality" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Date of Entry">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtDateEntry" runat="server" Width="359px">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorText="must enter correct date">
                                <RegularExpression ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ErrorText="must enter date only" />
                                <RequiredField IsRequired="True" ErrorText="must enter date only" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Date of Exit">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtDateExit" runat="server" Width="359px">
                            <ValidationSettings Display="Dynamic" EnableCustomValidation="True" ErrorDisplayMode="Text" ErrorText="must enter correct date">
                                <RegularExpression ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ErrorText="must enter date only" />
                                <RequiredField IsRequired="True" ErrorText="must enter date only" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="ID Cart Image">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxImage ID="ASPxImage1" runat="server" ShowLoadingImage="true">
                        </dx:ASPxImage>
                        <dx:ASPxImage ID="ASPxImage2" runat="server" ShowLoadingImage="True">
                        </dx:ASPxImage>
                        <dx:ASPxUploadControl ID="ASPxUploadControl1" runat="server" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" ShowUploadButton="True" UploadMode="Auto" Width="280px">
                        </dx:ASPxUploadControl>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Department">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtDepartment" runat="server" Width="359px">
                            <ValidationSettings>
                                <RequiredField IsRequired="True" ErrorText="must enter department" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Doctor">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtDoctor" runat="server" Width="359px">
                            <ValidationSettings>
                                <RequiredField IsRequired="True" ErrorText="must enter doctor name" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Phone">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtPhone" runat="server" Width="359px">
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Mobile">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxTextBox ID="txtMobile" runat="server" Width="359px">
                            <ValidationSettings>
                                <RegularExpression ValidationExpression="^01[0-9]{9}" ErrorText="must enter correct formate and start with 01" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">
                        <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="do you have a history of illness ?">
                        </dx:ASPxLabel>
                    </td>
                    <td style="margin: 15px; width: 402px; text-align: left;">
                        <asp:CheckBox ID="CheckBox1" runat="server" style="text-align: left" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="margin: 15px;">
                    <td colspan="3" style="margin: 15px;">
                        
                    </td>
                </tr>
                <tr style="margin: 15px;">
                    <td style="width: 199px; margin: 15px;">&nbsp;</td>
                    <td style="margin: 15px; width: 402px;">
                        <dx:ASPxButton ID="btnSave0" runat="server" CssClass="btn-primary" Font-Bold="True" Font-Size="Large" OnClick="btnSave_Click" Text="Save" Width="133px">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnCancel" runat="server" Font-Bold="True" Font-Size="Large" OnClick="btnCancel_Click" Text="Cancel" Visible="False" Width="127px">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnSaveChange" runat="server" Font-Bold="True" Font-Size="Large" OnClick="btnSaveChange_Click" Text="Save Change" Visible="False">
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <dx:ASPxValidationSummary ID="ASPxValidationSummary1" runat="server">
                        </dx:ASPxValidationSummary>
                    </td>
                </tr>
                <tr style="margin: 15px;">
                    <td colspan="3" style="margin: 15px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                Loading...
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    






</asp:Content>
