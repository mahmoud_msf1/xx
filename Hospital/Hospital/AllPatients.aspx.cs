﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hospital
{
    public partial class AllPatients : System.Web.UI.Page
    {
        BL.clsRegister obj = new BL.clsRegister();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            for (int i = 0; i < dgrShow.Rows.Count; i++)
            {
                if (DateTime.Now > Convert.ToDateTime(dgrShow.Rows[i].Cells[3].Text))
                {
                    dgrShow.Rows[i].Cells[5].Text = "yes";
                }
                else
                {
                    dgrShow.Rows[i].Cells[5].Text = "No";
                }
            }
        }

        protected void dgrShow_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = dgrShow.SelectedRow.Cells[1].Text;
            Response.Redirect("RegisterPatient.aspx?id=" + id);
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
     
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            dgrShow.DataSource = obj.Search(txtSearch.Text);
          //  dgrShow.DataBind();
        }
    }
}