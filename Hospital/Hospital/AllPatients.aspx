﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllPatients.aspx.cs" Inherits="Hospital.AllPatients" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <dx:ASPxButton ID="btnSearch" runat="server" Font-Bold="True" ForeColor="#009933" OnClick="btnSearch_Click" Text="Search">
    </dx:ASPxButton>
    <dx:ASPxTextBox ID="txtSearch" runat="server" Width="432px" Height="19px" OnTextChanged="txtSearch_TextChanged" ></dx:ASPxTextBox> <br /><br />
     <asp:GridView ID="dgrShow" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="dgrShow_SelectedIndexChanged" Width="540px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="EnglishName" HeaderText="EnglishName" SortExpression="EnglishName" />
            <asp:BoundField DataField="DateOfExit" HeaderText="DateOfExit" SortExpression="DateOfExit" />
            <asp:BoundField DataField="Doctor" HeaderText="Doctor" SortExpression="Doctor" />
            <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
            <asp:TemplateField HeaderText="IS Exit ?">
                <ItemTemplate>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="ASPxLabel">
                    </dx:ASPxLabel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MyHospitalDBConnectionString2 %>" SelectCommand="SELECT [ID], [EnglishName], [DateOfExit], [Doctor], [Department] FROM [tblNewRegister]"></asp:SqlDataSource>
    <br />
    <br />
    <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Register New Patient" Font-Bold="True" NavigateUrl="~/RegisterPatient.aspx">
    </dx:ASPxHyperLink>
</asp:Content>
