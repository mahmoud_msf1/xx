﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace Hospital
{
    public partial class RegisterPatient : System.Web.UI.Page
    {
        BL.clsRegister obj = new BL.clsRegister();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = obj.GetSelectedPatient(Convert.ToInt32(Request.QueryString["id"]));
            if (dt.Rows.Count > 0)
            {
                btnSaveChange.Visible = true;
                btnCancel.Visible = true;
                btnSave0.Visible = false;

                txtNameArabic.Text = dt.Rows[0][2].ToString();
                txtNameEnglish.Text = dt.Rows[0][1].ToString();
                rdoGender.SelectedItem.Text = dt.Rows[0][3].ToString();
                txtNationality.Text = dt.Rows[0][4].ToString();
                txtDateEntry.Text = dt.Rows[0][5].ToString();
                txtDateExit.Text = dt.Rows[0][6].ToString();
                txtDepartment.Text = dt.Rows[0][7].ToString();
                txtDoctor.Text = dt.Rows[0][8].ToString();
                txtPhone.Text = dt.Rows[0][9].ToString();
                txtMobile.Text = dt.Rows[0][10].ToString();


                txtNameArabic.ReadOnly = true;
                txtNameEnglish.ReadOnly = true;
                txtNationality.ReadOnly = true;
            }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {try
            {
                obj.NewRegister(txtNameEnglish.Text, txtNameArabic.Text, rdoGender.SelectedItem.Value.ToString(),
                    txtNationality.Text, txtDateEntry.Text, txtDateExit.Text, txtDepartment.Text,
                    txtDoctor.Text, txtPhone.Text, txtMobile.Text);


                lblMsg.Text = "Added Successfully";
                txtNameArabic.Text = "";
                txtNameEnglish.Text = "";
                txtNationality.Text = "";
                txtDateEntry.Text = "";
                txtDateExit.Text = "";
                txtDepartment.Text = "";
                txtDoctor.Text = "";
                txtPhone.Text = "";
                txtMobile.Text = "";
               
            }
            catch
            {
                lblMsg.Text = "Not Added !!";
            }
        }

        protected void ASPxUploadControl1_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
          
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnSaveChange_Click(object sender, EventArgs e)
        {
            try
            {
                obj.EditData(txtNameEnglish.Text, txtNameArabic.Text, txtDateEntry.Text,
                             txtDateExit.Text, txtDepartment.Text, txtDoctor.Text,
                             txtPhone.Text, txtMobile.Text);

                lblMsg.Text = "Edited Successfully";
                txtNameArabic.Text = "";
                txtNameEnglish.Text = "";
                txtNationality.Text = "";
                txtDateEntry.Text = "";
                txtDateExit.Text = "";
                txtDepartment.Text = "";
                txtDoctor.Text = "";
                txtPhone.Text = "";
                txtMobile.Text = "";
            }
            catch
            {
                lblMsg.Text = "Not Edited !!";
            }
        }

        
    }
}